module.exports = {
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
    "env": {
        "browser": true,
        "mocha": true,
    },
    "parser": "babel-eslint",
    "rules": {
        "strict": 0,
        "react/prefer-stateless-function": 0,
				"linebreak-style": 0,
				"no-mixed-spaces-and-tabs": "off",
				"no-multi-spaces": "off",
				"no-tabs": "off",
				"indent": ["off", "tab"],
				"space-before-function-paren": ["off", "always"],
				"quotes": ["off", "single", { "avoidEscape": true, "allowTemplateLiterals": true }],
				"semi": ["off", "never"],
				"no-trailing-spaces": "off",
				"no-multiple-empty-lines": ["error", { "max": 6, "maxEOF": 0 }],
				"padded-blocks": ["error", { "switches": "never" }],
				"eol-last": "off",
				"arrow-parens": ["off", "as-needed"],
				"comma-dangle": ["off", {
					"arrays": "never",
					"objects": "never",
					"imports": "never",
					"exports": "never",
					"functions": "ignore"
			}]
    },
};
