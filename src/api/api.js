export default class Api {

	static GetAlbumsList = () => {
    const data = [];
    const API_URL = 'https://jsonplaceholder.typicode.com/albums'
    return fetch(API_URL)
    .then(response => {
        return response.json()
    })
    .then(json => {
        const albums = json.map(({ userId, id, title }) => ({
            userId,
            id,
            title
        }))
        const promises = [];
        albums.forEach(album => {       // fetch album images for each album
            const fetchPromise = fetch('https://jsonplaceholder.typicode.com/albums/' + album.id + '/photos')
            .then(response => {
                return response.json();
            }).then(json => {
                data.push({
									userId: album.userId, id: album.id, title: album.title, albumThumbnails: [json[0].thumbnailUrl, json[1].thumbnailUrl, json[2].thumbnailUrl, json[3].thumbnailUrl, json[4].thumbnailUrl, json[5].thumbnailUrl]
								})
            })
            promises.push(fetchPromise)
        })
        return Promise.all(promises)
    })
    .then(() => {
        data.sort((a, b) => {
            return a.id - b.id     // sort by album id
        })
        return data;
    })
	}

	static GetAlbumImages = (album) => {
    const API_URL = 'https://jsonplaceholder.typicode.com/albums/' + album + '/photos'
    return fetch(API_URL).then(resp => {
        return resp.json()
    }).then(json => {
        return json.map(({ albumId, id, title, url, thumbnailUrl }) => ({
            albumId,
            id,
            title,
            url,
            thumbnailUrl
        }))
    })
	}

	static GetImage = (id) => {
    const API_URL = 'https://jsonplaceholder.typicode.com/photos/' + id
    return fetch(API_URL).then(resp => {
        return resp.json()
    })
	}

}
