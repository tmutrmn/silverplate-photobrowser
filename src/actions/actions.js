export const REQUEST_ALBUMS = 'REQUEST_ALBUMS'
export const RECEIVE_ALBUMS = 'RECEIVE_ALBUMS'

export const REQUEST_ALBUM = 'REQUEST_ALBUM'
export const RECEIVE_ALBUM = 'RECEIVE_ALBUM'

export const REQUEST_IMAGE = 'REQUEST_IMAGE'
export const RECEIVE_IMAGE = 'RECEIVE_IMAGE'


// all albums
export const requestAlbums = () => ({ type: REQUEST_ALBUMS })
export const receiveAlbums = data => ({ type: RECEIVE_ALBUMS, data })


// album
export const requestAlbum = (albumId) => ({
	type: REQUEST_ALBUM,
	albumId
})
export const receiveAlbum = data => ({ type: RECEIVE_ALBUM, data })


// image
export const requestImage = (imageId) => ({
	type: REQUEST_IMAGE,
	imageId
})
export const receiveImage = data => ({ type: RECEIVE_IMAGE, data })
