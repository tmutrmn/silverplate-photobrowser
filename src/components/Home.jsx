import React from 'react';

import AlbumList from "./AlbumList";

export default class Home extends React.PureComponent {
  render() {
    return (
      <div className="page-home">
				<h4>REACT PHOTOBROWSER</h4>
				<AlbumList />
      </div>
    );
  }
}
