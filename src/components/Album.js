import React from "react";
import { Link } from 'react-router'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { requestAlbum } from '../actions/actions'


class Album extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			id: this.props.params.albumId		// ROUTER V4 =  id: this.props.match.params.albumId
		};
	}

	componentDidMount() {
		this.props.requestAlbum(this.state.id)
	}

	image(image) {
		return (
			<div className="pure-u-1-4" key={image.id}>
				<figure>
					<Link to={'/image/' + image.id }>
						<img src={image.thumbnailUrl} key={image.id} alt={image.title} />
						<figcaption>
							<h4>{image.title}</h4>
							<button className="button-c pure-button">See full image</button>
						</figcaption>
					</Link>
				</figure>
				<br />
			</div>
		)
	}

	render() {
		return (
			<div>
				<br />
				<button onClick={this.props.router.goBack}>Go Back</button>
				<br />
				<h1>Photos in album number: {this.state.id}</h1>
				<div className="pure-g">
				{ this.props.data.length ? this.props.data.map(image => this.image(image)) : <pre>Loading images...</pre> }
				</div>
			</div>
		)
	}

}


	const mapStateToProps = state => ({ id: state.id, data: state.data })

	const mapDispatchToProps = dispatch =>
			bindActionCreators({ requestAlbum }, dispatch)


	export default connect(mapStateToProps, mapDispatchToProps)(Album)
