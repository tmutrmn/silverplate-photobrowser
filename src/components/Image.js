import React from "react";

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'


import { requestImage } from '../actions/actions'



class Image extends React.Component {

		constructor(props) {
			super(props);
			this.state = {
				id: this.props.params.imageId		// ROUTER V4 = id: this.props.match.params.imageId
			};
		}

		componentDidMount() {
				this.props.requestImage(this.state.id)
		}

		image(image) {
			return (
				<figure>
					<img src={image.url} key={image.id} alt={image.title} />
					<figcaption>
						<h2>{image.title}</h2>
						<p>Belongs to album number: {image.albumId} </p>
						<button onClick={this.props.router.goBack}>Go Back</button>
					</figcaption>
				</figure>
			)
		}


		render() {
				return (
					<div className="pure-g">
						<div className="pure-u-1-1" key={this.props.id}>

							{ (typeof this.props.data != "undefined") ? this.image(this.props.data) : <pre>Loading image...</pre> }

						</div>
					</div>
				)
		}

}


const mapStateToProps = state => ({ id: state.id, data: state.data })

const mapDispatchToProps = dispatch =>
		bindActionCreators({ requestImage }, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Image)
