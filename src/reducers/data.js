import { RECEIVE_ALBUMS, RECEIVE_ALBUM, RECEIVE_IMAGE } from '../actions/actions'

export default (state = {}, { type, data }) => {
	switch (type) {
		case RECEIVE_ALBUMS:
			return data
		case RECEIVE_ALBUM:
			return data
		case RECEIVE_IMAGE:
			return data
		default:
			return state
	}
}
