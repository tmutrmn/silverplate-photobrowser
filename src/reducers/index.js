import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
// import { reducer as formReducer } from 'redux-form';
import data from './data'

// main reducers
const reducers = combineReducers({
  routing: routerReducer,
  // form: formReducer,
	// your reducer here
	data
});



export default reducers
