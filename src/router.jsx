import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { history } from './store';
import App from './components/App';
import Home from './components/Home';
import AlbumList from "./components/AlbumList";
import Album from "./components/Album";
import Image from "./components/Image";
import NotFound from './components/NotFound';

// build the router
/*
<Route path="/image/:imageId" component={Image} />
			<Route path="/about" component={About} />
			*/
export default (
  <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
    <Route path="/" component={App}>
			<IndexRoute component={Home} />
			<Route exact path="/" component={Home} />
			<Route exact path="/albums" component={AlbumList} />
			<Route path="/album/:albumId" component={Album} />
			<Route path="/image/:imageId" component={Image} />
      <Route path="*" component={NotFound} />
    </Route>
  </Router>
);
