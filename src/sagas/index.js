import { call, put, takeLatest, fork } from 'redux-saga/effects'

import {	REQUEST_ALBUMS, RECEIVE_ALBUMS,
					REQUEST_ALBUM, RECEIVE_ALBUM,
					REQUEST_IMAGE, RECEIVE_IMAGE } from '../actions/actions'
// import { GetAlbums, GetAlbumImages, GetImage } from '../api/api'
import Api from '../api/api'

function* getAlbumsData(action) {   // worker saga
	try {
		const data = yield call(Api.GetAlbumsList);   // do api call
    yield put({ type: RECEIVE_ALBUMS, data })
	} catch (e) {
		console.log(e)
  }
}


function* getAlbumData(action) {   // worker saga
	try {
    const data = yield call(Api.GetAlbumImages, action.albumId);    // do api call
    yield put({ type: RECEIVE_ALBUM, data });
	} catch (e) {
		console.log(e)
  }
}


function* getImageData(action) {   // worker saga
	try {
    const data = yield call(Api.GetImage, action.imageId);    // do api call
    yield put({ type: RECEIVE_IMAGE, data });
	} catch (e) {
		console.log(e)
  }
}


// main saga generators
export default function* sagas() {
  yield [
    fork(takeLatest, REQUEST_ALBUMS, getAlbumsData),
    fork(takeLatest, REQUEST_ALBUM, getAlbumData),
    fork(takeLatest, REQUEST_IMAGE, getImageData),
  ];
}
